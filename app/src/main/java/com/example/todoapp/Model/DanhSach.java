package com.example.todoapp.Model;

public class DanhSach {
    private int id;
    private String mName;


    public DanhSach(int id, String mName) {
        this.mName = mName;
        this.id = id;
    }

    public String getmName() {
        return mName;
    }

    public int getId() {
        return id;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public void setId(int id) {
        this.id = id;
    }
}
